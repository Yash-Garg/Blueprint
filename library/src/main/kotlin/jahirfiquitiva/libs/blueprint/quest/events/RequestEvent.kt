package jahirfiquitiva.libs.blueprint.quest.events

/**
 * Created by Allan Wang on 2016-08-27.
 */
class RequestEvent(val isPreparing: Boolean, val isSent: Boolean, val exception: Exception?)
