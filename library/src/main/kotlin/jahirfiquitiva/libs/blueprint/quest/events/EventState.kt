package jahirfiquitiva.libs.blueprint.quest.events

/**
 * Created by Allan Wang on 2016-08-27.
 */
enum class EventState {
    DISABLED, ENABLED, STICKIED
}
